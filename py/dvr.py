#!/usr/bin/env python

from __future__ import annotations

import asyncio
from datetime import timedelta
from pathlib import Path
from typing import Optional

import aiorun
from colorama import Fore, init
from devtools import debug

from activity_queue import (ActivityQueue, async_q_tee, log_activity,
                            log_inactivity)
from aiopynput import AioPynput
from aiowatchdog import AioWatchdog
from api import serve_api
from db import DB, get_db
from monitored_subprocess import MonitoredSubprocess
from ocr import process_ocr_queue
from retention import enforce_retention
from video_recorder import SegmentFile, VideoRecorder


async def watch_for_segments(
    output_directory: Path, db: DB, ocr_q: asyncio.Queue
) -> None:
    # We don't really know the name of the files that ffmpeg creates
    # (since we use a template), so we watch the directory for them
    watcher = AioWatchdog(directory=output_directory).start()
    try:
        current_segment: Optional[SegmentFile] = None
        while True:
            evt = await watcher.async_q.get()
            if Path(evt.src_path).suffix == ".mp4":
                if evt.event_type == "created":
                    print(Fore.RED + f"Created {evt.src_path}")
                    current_segment = SegmentFile(Path(evt.src_path))
                elif evt.event_type == "modified" and current_segment is not None:
                    if current_segment.is_closed:
                        print(Fore.RED + f"Closed {current_segment}")
                        db.insert_segment(current_segment)
                        await ocr_q.put(current_segment)
                        # No double inserts
                        current_segment = None
    except asyncio.CancelledError:
        watcher.stop()


def cleanup_truncated_segments(output_directory: Path, db: DB) -> None:
    # This should not be run while the video recorder is active!

    existing_segments = {
        r["filename"] for r in db.execute("SELECT filename FROM segments").fetchall()
    }
    for file in output_directory.glob("*.mp4"):
        # We skip existing segments to avoid re-ffprobing all files
        if file.name in existing_segments:
            continue

        seg = SegmentFile(file)
        if not seg.is_closed:
            debug("Unlinking truncated segment", file)
            file.unlink()


async def record_video(activity_q: asyncio.Queue, output_directory: Path, db: DB):
    cleanup_truncated_segments(output_directory, db)
    recorder = VideoRecorder(output_directory=output_directory, segment_duration=30)
    try:
        # Assume activity at start
        await recorder.start()
        while True:
            ts, evt, *_ = await activity_q.get()
            if evt == "activity" and not recorder.is_active:
                await recorder.start()
            elif evt == "inactivity" and recorder.is_active:
                await recorder.stop()
            await recorder.check()
    except asyncio.CancelledError:
        if recorder.is_active:
            await recorder.stop()


async def run_active_window_watcher(output_directory: Path) -> None:
    try:
        proc: MonitoredSubprocess = MonitoredSubprocess(
            "active_window_watcher",
            await asyncio.create_subprocess_exec(
                "node",
                "../node/build/index.js",
                stdin=asyncio.subprocess.DEVNULL,
                stderr=(output_directory / "active_window_watcher.stderr").open("w"),
                stdout=(output_directory / "active_window_watcher.stdout").open("w"),
            ),
        )
        await proc.wait()
    except asyncio.CancelledError:
        print(Fore.GREEN + "Shutting down active_window watcher")
        await proc.stop()


async def queue_missed_segments_for_ocr(
    db: DB, output_directory: Path, ocr_q: asyncio.Queue
) -> None:
    segments = [
        SegmentFile(output_directory / r["filename"])
        for r in db.execute(
            "SELECT filename FROM segments WHERE ocr_done_at IS NULL ORDER BY created_at ASC"
        ).fetchall()
        if (output_directory / r["filename"]).exists()
    ]
    if len(segments):
        debug("Queuing for OCR", segments)
        await asyncio.gather(*(ocr_q.put(s) for s in segments))


async def main() -> None:
    try:
        init(autoreset=True)
        pynput_q: AioPynput = AioPynput().start()
        activity_q: ActivityQueue = ActivityQueue(
            events_queue=pynput_q.async_q, inactivity_window=10
        )

        output_directory = Path("tmp")
        output_directory.mkdir(parents=True, exist_ok=True)

        with get_db(output_directory / "db.sqlite") as db:
            inq, acq, vidq = async_q_tee(activity_q.queue, n=3)
            ocr_q: asyncio.Queue = asyncio.Queue()
            await asyncio.gather(
                serve_api(db),
                enforce_retention(
                    retention=timedelta(hours=24),
                    output_directory=output_directory,
                    db=db,
                    every=60 * 30,
                ),
                activity_q.run(),
                log_inactivity(inq),
                log_activity(acq),
                record_video(vidq, output_directory, db),
                queue_missed_segments_for_ocr(db, output_directory, ocr_q),
                watch_for_segments(output_directory, db, ocr_q),
                process_ocr_queue(ocr_q, db, output_directory),
                run_active_window_watcher(output_directory),
            )
    except asyncio.CancelledError:
        print(Fore.GREEN + "Shutting down async loop")
        pynput_q.stop()
        print(Fore.GREEN + "Done shutdown")


if __name__ == "__main__":
    aiorun.run(main(), stop_on_unhandled_errors=True)
