#!/usr/bin/env python

import asyncio
import concurrent
from contextlib import contextmanager
from datetime import datetime, timedelta
from pathlib import Path
from typing import Generator, List, Literal, Optional, Tuple

import click
from devtools import debug

from db import DB, get_db
from google_ocr import GoogleOcr
from ocr_result import OcrResult
from ocr_utils import (ActiveWindowFrameSelector, Ocrer,
                       PeriodicFrameSelector, FrameSelector,
                       screenshots_at)
from tesseract_ocr import TesseractOcr
from video_recorder import SegmentFile


@contextmanager
def _with_google(db: DB) -> Generator[Tuple[Ocrer, FrameSelector], None, None]:
    with concurrent.futures.ThreadPoolExecutor() as executor:
        yield (GoogleOcr(executor), ActiveWindowFrameSelector(db))


@contextmanager
def _with_tesseract() -> Generator[Tuple[Ocrer, FrameSelector], None, None]:
    yield (TesseractOcr(), PeriodicFrameSelector(timedelta(seconds=5)))


async def process_ocr_queue(q: asyncio.Queue, db: DB, output_directory: Path) -> None:
    screenshot_directory = output_directory / "ocr_screenshots"
    screenshot_directory.mkdir(parents=True, exist_ok=True)
    with _with_tesseract() as (ocrer, frame_selector):
        try:
            while True:
                segment: SegmentFile = await q.get()

                # Retention could clean up segments before we get here
                if not segment.filename.exists():
                    debug('Segment disappeared', segment.filename)
                    continue

                await process_one_segment(
                    ocrer, frame_selector, segment, db, screenshot_directory,
                )
                db.update_ocr_done(segment)
        except asyncio.CancelledError:
            pass


async def ocr_segment_frame(
    ocrer: Ocrer, imgfile: Path, timestamp: datetime,
) -> OcrResult:
    ocr_text = await ocrer.ocr(imgfile)
    return OcrResult(text=ocr_text, timestamp=timestamp, imgfile=imgfile)


async def process_one_segment(
    ocrer: Ocrer,
    frame_selector: FrameSelector,
    segment: SegmentFile,
    db: DB,
    screenshot_directory: Optional[Path] = None,
) -> None:
    frame_timestamps: List[datetime] = list(frame_selector.get_timestamps(segment))
    async with screenshots_at(
        segment, frame_timestamps, screenshot_directory
    ) as frames:
        results: List[OcrResult] = await asyncio.gather(
            *(ocr_segment_frame(ocrer, imgfile, ts) for ts, imgfile in frames)
        )

    for result in results:
        db.insert_ocr_result(segment, result)


@click.command()
@click.argument(
    "segment_file", type=click.Path(exists=True, dir_okay=False, file_okay=True)
)
@click.argument(
    "output_directory", type=click.Path(exists=True, dir_okay=True, file_okay=False)
)
@click.option("--ocr", type=click.Choice(("google", "tesseract")), default="tesseract")
def main(
    segment_file: str, output_directory: str, ocr: Literal["google", "tesseract"]
) -> None:
    seg = SegmentFile(Path(segment_file))
    output_dir: Path = Path(output_directory)
    with get_db(output_dir / "db.sqlite") as db:
        if ocr == "google":
            cm = _with_google(db)
        elif ocr == "tesseract":
            cm = _with_tesseract()

        with cm as (ocrer, frame_selector):
            asyncio.run(process_one_segment(ocrer, frame_selector, seg, db))


if __name__ == "__main__":
    main()
