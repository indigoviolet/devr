import asyncio
import tempfile
from contextlib import AsyncExitStack, asynccontextmanager
from datetime import datetime, timedelta
from pathlib import Path
from typing import (AsyncGenerator, Generator, Iterable, List, Optional,
                    Protocol, Tuple)

import attr
from devtools import debug

from db import DB
from monitored_subprocess import make_options_list
from video_recorder import SegmentFile


class Ocrer(Protocol):
    async def ocr(self, imgfile: Path) -> str:
        ...


def _format_offset(td: timedelta) -> str:
    hours, remainder = divmod(td.seconds, 3600)
    minutes, remainder = divmod(remainder, 60)
    return f"{hours:02d}:{minutes:02d}:{remainder:02d}.{td.microseconds}"


@asynccontextmanager
async def screenshot(
    segment: SegmentFile, timestamp: datetime, screenshot_directory: Optional[Path]
) -> AsyncGenerator[Path, None]:
    assert segment.starttime <= timestamp < segment.endtime, "Timestamp is in segment"
    offset = timestamp - segment.starttime
    try:
        with tempfile.NamedTemporaryFile(
            suffix=".png",
            dir=screenshot_directory,
            delete=(screenshot_directory is None),
        ) as imgfile:
            cmd = [
                "ffmpeg",
                "-y",
                *make_options_list(
                    ss=_format_offset(offset),
                    i=str(segment.filename),
                    vframes=1,
                    **{"q:v": 1},
                ),
                imgfile.name,
            ]
            proc = await asyncio.subprocess.create_subprocess_exec(
                *cmd,
                stdin=asyncio.subprocess.DEVNULL,
                stderr=asyncio.subprocess.DEVNULL,
                stdout=asyncio.subprocess.DEVNULL,
            )
            await proc.wait()
            if proc.returncode != 0:
                raise RuntimeError(f"Failed to screenshot {segment} {offset}")
            yield Path(imgfile.name)
    except asyncio.CancelledError:
        # Necessary?
        imgfile.close()


@asynccontextmanager
async def screenshots_at(
    segment: SegmentFile,
    timestamps: Iterable[datetime],
    screenshot_directory: Optional[Path],
) -> AsyncGenerator[Iterable[Tuple[datetime, Path]], None]:
    async with AsyncExitStack() as stack:
        imgfiles = await asyncio.gather(
            *(
                stack.enter_async_context(screenshot(segment, ts, screenshot_directory))
                for ts in timestamps
            )
        )
        yield list(zip(timestamps, imgfiles))


def _datetime_range(
    start: datetime, stop: datetime, step: timedelta
) -> Generator[datetime, None, None]:
    cursor = start
    while cursor < stop:
        yield cursor
        cursor += step


class FrameSelector(Protocol):
    def get_timestamps(self, segment: SegmentFile) -> Iterable[datetime]:
        ...


@attr.s
class ActiveWindowFrameSelector:
    db: DB = attr.ib()
    _most_recent_timestamp: Optional[datetime] = attr.ib(init=False, default=None)

    def get_timestamps(self, segment: SegmentFile) -> Iterable[datetime]:
        timestamps = list(
            _get_screenshot_timestamps(segment, self.db, self._most_recent_timestamp)
        )
        self._most_recent_timestamp = max(timestamps)
        return timestamps


@attr.s
class PeriodicFrameSelector:
    step: timedelta = attr.ib()

    def get_timestamps(self, segment: SegmentFile) -> Iterable[datetime]:
        return _datetime_range(segment.starttime, segment.endtime, self.step)


def _get_screenshot_timestamps(
    segment: SegmentFile,
    db: DB,
    most_recent_ocr_timestamp: Optional[datetime],
    min_interval: timedelta = timedelta(seconds=30),
    max_interval: timedelta = timedelta(seconds=60),
) -> Generator[datetime, None, None]:

    """
    The goal is to try and maximize information/screenshot - since we
    don't have continuous OCR.

    So we will try to use active_windows as a source for deciding when
    to screenshot. Note also that screenshots are not per-window, so
    the active window could already be on screen -- this isn't easily
    solved.

    Algorithm:

    Input: segment

    1. Find most recent OCR timestamp -> most_recent
    2. Find active_window timestamps in segment (starttime or endtime or midtime)
    3. starting from most_recent, if next active_window is within the allowed [min, max] range, pick it
    4. else pick max
    """

    cursor = most_recent_ocr_timestamp
    for aw_ts in _get_active_window_timestamps(segment, db):
        if cursor is None:
            cursor = aw_ts
            yield _adjust_cursor(segment, cursor)
        elif (aw_ts - cursor) > max_interval:
            # We know this is in the segment because aw_ts is in the segment and aw_ts > cursor + max_interval
            cursor += max_interval
            yield cursor
        elif (aw_ts - cursor) < min_interval:
            pass

    # If we didn't update at all, pick the last frame
    if cursor == most_recent_ocr_timestamp:
        yield _adjust_cursor(segment, segment.endtime - timedelta(seconds=1))


def _get_active_window_timestamps(segment: SegmentFile, db: DB) -> List[datetime]:
    result = db.conn.execute(
        # endtime is a computed column in a VIEW which doesn't retain the TIMESTAMP type
        "SELECT endtime FROM active_windows_ranges WHERE endtime > ? AND endtime < ?",
        (segment.starttime, segment.endtime,),
    )
    return [datetime.fromisoformat(r["endtime"]) for r in result]


def _adjust_cursor(segment: SegmentFile, timestamp: datetime) -> datetime:
    result: datetime = max(timestamp - timedelta(seconds=1), segment.starttime)
    return result
