from fastapi.applications import FastAPI

from devtools import debug
import uvicorn
from db import DB, Window
api: FastAPI = FastAPI()


@api.post('/active_window/')
async def insert_active_window(win: Window):
    # Making this not-async will break with
    #
    # > sqlite3.ProgrammingError: SQLite objects created in a thread can only be used in that same thread
    #
    # because Starlette will try to run this in a thread executor
    #
    db: DB = api.state.db
    db.insert_active_window(win)

class _CustomServer(uvicorn.Server):
    def install_signal_handlers(self):
        pass

async def serve_api(db: DB) -> None:
    api.state.db = db

    cfg = uvicorn.Config(api, loop="none", lifespan="off")
    server = _CustomServer(cfg)
    await server.serve()
