#!python

from __future__ import annotations
import asyncio
from pathlib import Path

import attr
import janus
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer
import click
import aiorun

@attr.s(eq=False)
class Handler(FileSystemEventHandler):
    _queue = attr.ib()

    def on_any_event(self, event):
        self._queue.put_nowait(event)


@attr.s
class AioWatchdog:
    directory: Path = attr.ib(kw_only=True)
    recursive: bool = attr.ib(kw_only=True, default=False)
    _observer: Observer = attr.ib(init=False)
    _queue: janus.Queue = attr.ib(init=False)

    def __attrs_post_init__(self):
        self._queue = janus.Queue(loop=asyncio.get_running_loop())

    def start(self) -> AioWatchdog:
        self._observer = Observer()
        self._observer.schedule(
            Handler(self.sync_q), str(self.directory), recursive=self.recursive
        )
        self._observer.start()
        return self

    def stop(self):
        self._observer.stop()
        self._observer.join()

    @property
    def sync_q(self):
        return self._queue.sync_q

    @property
    def async_q(self):
        return self._queue.async_q


async def _main(directory: Path):
    wd = AioWatchdog(directory=directory).start()
    try:
        while True:
            evt = await wd.async_q.get()
            print(evt)
    except asyncio.CancelledError:
        wd.stop()


@click.command()
@click.argument("directory", type=click.Path(file_okay=False, dir_okay=True, exists=True))
def main(directory):
    aiorun.run(_main(Path(directory)))


if __name__ == "__main__":
    main()
