#!/usr/bin/env python

import sqlite3
import subprocess
from pathlib import Path
from typing import Dict

import click
from devtools import debug
from tabulate import tabulate

from debug_conn import DebugConn


def _match_active_windows_sql(querystr: str) -> str:
    return f"""
WITH
active_windows_matches
AS (
   SELECT rowid, title, appname FROM active_windows_fts where active_windows_fts MATCH '{querystr}'
)
SELECT
'active_window' AS match_type,
PRINTF('%s : %s', l.appname, l.title) AS item, starttime, endtime
FROM active_windows_matches l JOIN active_windows_ranges r ON (l.rowid = r.rowid)
    """


def _match_ocr_sql(querystr: str) -> str:
    return f"""
    SELECT
    'ocr' AS match_type,
    REPLACE(SNIPPET(ocr_fts, -1, '<', '>', '...', 5), '\n', ' ') AS item,
    timestamp AS starttime,
    timestamp AS endtime
    FROM ocr_fts where ocr_fts MATCH '{querystr}'
    """


def _union_all(**queries: str) -> str:
    return "\n UNION ALL \n".join(
        f"SELECT match_type, item, starttime, endtime FROM ({q}) {name}" for name, q in queries.items()
    )


def matching_timeranges_sql(querystr: str) -> str:
    return _union_all(
        active_windows=_match_active_windows_sql(querystr), ocr=_match_ocr_sql(querystr)
    )


def overlapping_segments_sql(timeranges_sql: str) -> str:
    return f"""
WITH
matching_ranges
AS (
   {timeranges_sql}
)
SELECT s.filename, m.*
FROM matching_ranges m, segments s
WHERE NOT ((s.endtime < m.starttime) OR (s.starttime > m.endtime))
ORDER BY s.filename ASC, m.starttime ASC
    """


@click.command()
@click.argument("output_directory", type=click.Path(file_okay=False, dir_okay=True))
@click.argument("querystr", type=str)
def main(output_directory: str, querystr):
    conn: sqlite3.Connection = DebugConn(
        sqlite3.connect(Path(output_directory) / "db.sqlite")
    )
    sql = overlapping_segments_sql(matching_timeranges_sql(querystr))
    results = conn.execute(sql)
    header = [c[0] for c in results.description]
    rows = results.fetchall()
    if len(rows):
        # Run an fzf filter so we can play videos from the output (using mpv, for the OSD)
        input = tabulate([header] + rows, tablefmt="plain")
        proc = subprocess.Popen(
            [
                "fzf",
                "--phony",
                "--bind",
                f"enter:execute(mpv --osd-level=2 {output_directory}/{{1}})",
                "--header-lines=1",
                "--no-sort",
                "--tac",
            ],
            stdin=subprocess.PIPE,
        )
        proc.communicate(input.encode("utf-8"))
    else:
        print('No matches')
    conn.close()


if __name__ == "__main__":
    main()
