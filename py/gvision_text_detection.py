#!python

import pickle

import click
from google.cloud import vision


@click.command()
@click.argument("imagefile", type=click.File("rb"))
def detect_text(imagefile):
    content = imagefile.read()
    image = vision.types.Image(content=content)
    client = vision.ImageAnnotatorClient.from_service_account_json(
        "devr-1468e8d00f0e.json"
    )
    response = client.text_detection(image=image)
    pickle.dump(response, open(f"{imagefile.name}.vision_response.pkl", "wb"))


if __name__ == "__main__":
    detect_text()
