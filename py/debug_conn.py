import wrapt
from devtools import debug


class DebugConn(wrapt.ObjectProxy):
    def execute(self, *args, **kwargs):
        debug(args, kwargs)
        return self.__wrapped__.execute(*args, **kwargs)

    def cursor(self):
        return DebugConn(self.__wrapped__.cursor())
