import asyncio
from datetime import datetime, timedelta
from pathlib import Path
from typing import Generator

from devtools import debug
from tzlocal import get_localzone
from pytz import utc

from db import DB


async def enforce_retention(
    retention: timedelta, output_directory: Path, db: DB, every: int
) -> None:
    while True:
        await _remove_data_older_than(
            compute_sliding_window_start(retention), output_directory, db
        )
        await asyncio.sleep(every)


async def _remove_data_older_than(
    timestamp: datetime, output_directory: Path, db: DB
) -> None:
    for seg in _segments_older_than(timestamp, output_directory):
        debug("Unlinking old segment", seg)
        seg.unlink()
    db.remove_data_older_than(timestamp)


def _segments_older_than(
    timestamp: datetime, output_directory: Path
) -> Generator[Path, None, None]:
    for seg in output_directory.glob("*.mp4"):
        stat = seg.stat()
        mtime = datetime.fromtimestamp(stat.st_mtime_ns / 1e9, tz=get_localzone())
        if mtime < timestamp:
            yield seg


def compute_sliding_window_start(retention: timedelta) -> datetime:
    return (datetime.now(get_localzone()) - retention).astimezone(utc)
