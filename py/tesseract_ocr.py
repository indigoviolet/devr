import asyncio
import multiprocessing
from asyncio import Semaphore
from pathlib import Path
from typing import Optional

import attr
from devtools import debug


@attr.s
class TesseractOcr:
    num_processes: int = attr.ib(default=multiprocessing.cpu_count() // 2)
    # If the semaphore gets created outside of asyncio.run(), then it
    # will get one loop (the default), and asyncio.run will create a
    # new loop, and we'll get a "attached to different loop" error. So
    # we will create it lazily (https://stackoverflow.com/a/55918049)
    _sem: Optional[Semaphore] = attr.ib(init=False, default=None)

    async def ocr(self, imgfile: Path) -> str:
        if self._sem is None:
            self._sem = Semaphore(self.num_processes)

        async with self._sem:
            # Hardcoding dpi for now
            cmd = f"convert {imgfile} -negate -resize 300% -type Grayscale - | tesseract - - --oem 3 --dpi 72"
            debug(cmd)
            proc = await asyncio.subprocess.create_subprocess_shell(
                cmd, stdout=asyncio.subprocess.PIPE
            )
            await proc.wait()
            result, _ = await proc.communicate()
            return result.decode("utf-8")
