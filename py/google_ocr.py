import asyncio
import concurrent
from pathlib import Path

import attr
from devtools import debug
from google.cloud import vision


@attr.s
class GoogleOcr:
    executor: concurrent.futures.Executor = attr.ib()

    async def ocr(self, imgfile: Path) -> str:
        result: str = await asyncio.get_event_loop().run_in_executor(
            self.executor, self._ocr_sync, imgfile
        )
        return result

    def _ocr_sync(self, imgfile: Path):
        debug(f"OCR for {imgfile}")
        content = imgfile.read_bytes()
        image = vision.types.Image(content=content)
        client = vision.ImageAnnotatorClient.from_service_account_json(
            "devr-1468e8d00f0e.json"
        )
        response = client.text_detection(image=image)
        return self._text_from_google_ocr_response(response)

    def _text_from_google_ocr_response(
        self, response: vision.types.AnnotateImageResponse
    ) -> str:
        x: str = response.text_annotations[0].description
        return x
