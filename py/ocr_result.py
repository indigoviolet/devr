from datetime import datetime
from pathlib import Path

import attr


@attr.s
class OcrResult:
    text: str = attr.ib()
    timestamp: datetime = attr.ib()
    imgfile: Path = attr.ib()
