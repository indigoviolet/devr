from __future__ import annotations

import sqlite3
from contextlib import closing, contextmanager
from datetime import datetime, timedelta
from pathlib import Path
from typing import (Any, Dict, Generator, Iterable, List, Literal, Optional,
                    Union)

import attr
import pydantic
import pytz

from debug_conn import DebugConn
from ocr_result import OcrResult
from video_recorder import SegmentFile


class Window(pydantic.BaseModel):
    window_id: int
    pid: int
    title: str
    appname: str
    path: str
    active_at: datetime


@attr.s(auto_attribs=True)
class Table:
    name: str
    columns: List[Column]
    fts: bool = True


@attr.s(auto_attribs=True)
class Column:
    name: str
    type: str
    indexed: bool = True


InsertFallback = Literal["REPLACE", "IGNORE", "ROLLBACK"]


def insert_sql(tablename: str, values: Dict[str, Any], fallback: InsertFallback) -> str:
    column_string = ", ".join(values.keys())
    placeholders = ", ".join("?" for _ in values)
    return (
        f"INSERT OR {fallback} INTO {tablename}({column_string}) VALUES({placeholders})"
    )


def update_sql(tablename: str, values: Dict[str, str], where: Dict[str, Any]) -> str:
    set_clause = ", ".join(f"{k} = ?" for k in values.keys())
    where_clause = ", ".join(f"{k} = ?" for k in where.keys())
    return f"UPDATE {tablename} SET {set_clause} WHERE {where_clause}"


def create_table_sql(table: Table) -> str:
    column_defs = ",\n".join(f"{col.name} {col.type}" for col in table.columns)
    return f"""CREATE TABLE IF NOT EXISTS {table.name} (
        {column_defs}
    )"""


def create_fts_sql(table: Table) -> List[str]:
    column_defs = ", ".join(
        f"{col.name}{' UNINDEXED' if not col.indexed else ''}" for col in table.columns
    )
    return (
        [
            f"""
            CREATE VIRTUAL TABLE IF NOT EXISTS {table.name}_fts
            USING fts5({column_defs}, content='{table.name}');
            """
        ]
        + create_fts_triggers_sql(table)
    )


def create_fts_triggers_sql(table: Table) -> List[str]:
    colnames = ["rowid"] + [col.name for col in table.columns]
    cols = ", ".join(f"{col}" for col in colnames)
    new_cols = ", ".join(f"NEW.{col}" for col in colnames)
    old_cols = ", ".join(f"OLD.{col}" for col in colnames)
    return [
        f"""
        CREATE TRIGGER IF NOT EXISTS {table.name}_ai AFTER INSERT ON {table.name} BEGIN
               INSERT INTO {table.name}_fts({cols}) VALUES ({new_cols});
        END;
        """,
        f"""
        CREATE TRIGGER IF NOT EXISTS {table.name}_ad AFTER DELETE ON {table.name} BEGIN
               INSERT INTO {table.name}_fts({table.name}_fts, {cols}) VALUES('delete', {old_cols});
        END;""",
        f"""
        CREATE TRIGGER IF NOT EXISTS {table.name}_au AFTER UPDATE ON {table.name} BEGIN
               INSERT INTO {table.name}_fts({table.name}_fts, cols) VALUES('delete', {old_cols});
               INSERT INTO {table.name}_fts(rowid, cols) VALUES ({new_cols});
        END;
        """,
    ]


@contextmanager
def get_db(db_file: Union[str, Path]) -> Generator[DB, None, None]:
    d = DB(db_file).connect().setup()
    with closing(d):
        yield d


def now_for_db() -> datetime:
    return datetime.now().astimezone(pytz.UTC)


@attr.s
class DB:
    db_file: Union[str, Path] = attr.ib()  # so that we can use :memory:
    _conn: Optional[sqlite3.Connection] = attr.ib(init=False, default=None)

    def connect(self) -> DB:
        # isolation_level + wal:
        #
        # https://docs.python.org/3/library/sqlite3.html#controlling-transactions
        # https://charlesleifer.com/blog/going-fast-with-sqlite-and-python/
        #
        # detect_types and TIMESTAMP types:
        #
        # We could use TIMESTAMP as a type for datetime columns and
        # have sqlite3 automatically convert these back and forth, but
        # this appears to be leaky:
        #
        # - the deserialized datetimes are naive (no timezone)
        #
        # - computed columns in VIEWs don't maintain their TIMESTAMP
        #   identity and so you have to explicitly specify `AS "column
        #   [TIMESTAMP]"` to get them to deserialize
        self._conn = DebugConn(
            sqlite3.connect(
                str(self.db_file),
                isolation_level=None,
                detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES,
            )
        )
        self._conn.row_factory = sqlite3.Row

        for p in self._pragmas:
            self.execute(f"pragma {p}")

        # TODO (extensions):
        #
        # - enable_load_extension is not available on all Pythons, see https://stackoverflow.com/a/59963751
        # - verify that we have JSON1 and FTS5 enabled/loaded, or do so
        return self

    @property
    def _pragmas(self) -> List[str]:
        return [
            # to allow multiple writers
            "journal_mode=wal",
            # delete triggers for replace
            "recursive_triggers=on",
        ]

    @property
    def conn(self) -> sqlite3.Connection:
        assert self._conn is not None, "Connected"
        return self._conn

    def setup(self) -> DB:
        self._make_tables()
        self._make_views()
        return self

    def insert_segment(self, segment: SegmentFile):
        self._insert_row(
            "segments",
            dict(
                filename=segment.filename.name,
                starttime=segment.starttime,
                endtime=segment.endtime,
                duration=segment.duration,
            ),
        )

    def insert_active_window(self, active_window: Window):
        self._insert_row(
            "active_windows",
            {**active_window.dict(), "active_at": active_window.active_at},
        )

    def insert_ocr_result(self, segment: SegmentFile, ocr_result: OcrResult):
        self._insert_row(
            "ocr",
            {
                "extracted_text": ocr_result.text,
                "timestamp": ocr_result.timestamp,
                "imagefile": ocr_result.imgfile.name,
                "segmentfile": segment.filename.name,
            },
            fallback="REPLACE",
        )

    def update_ocr_done(self, segment: SegmentFile):
        self._update_row(
            "segments",
            dict(ocr_done_at=now_for_db()),
            where={"filename": segment.filename.name},
        )

    def _update_row(
        self, tablename: str, values: Dict[str, Any], where: Dict[str, Any]
    ) -> None:
        values = {
            "updated_at": now_for_db(),
            **values,
        }
        self.execute(
            update_sql(tablename, values=values, where=where),
            tuple(values.values()) + tuple(where.values()),
        )

    def _insert_row(
        self,
        tablename: str,
        values: Dict[str, Any],
        fallback: InsertFallback = "ROLLBACK",
    ) -> None:
        # set default value for created_at, so that we can use a
        # uniform format for all timestamps, CURRENT_TIMESTAMP is not
        # timezone-aware -- assuming every table will want created_at
        values = {
            "created_at": now_for_db(),
            **values,
        }
        self.execute(
            insert_sql(tablename, values, fallback=fallback), tuple(values.values()),
        )

    def _make_tables(self) -> None:
        for tab in self._table_defs():
            self.execute(create_table_sql(tab))
            if tab.fts:
                for statement in create_fts_sql(tab):
                    self.conn.execute(statement)

    def _make_views(self) -> None:
        for v in self._view_defs():
            self.execute(v)

    def _table_defs(self) -> List[Table]:
        return [
            Table(
                "segments",
                [
                    Column("filename", "TEXT NOT NULL UNIQUE"),
                    Column("starttime", "TEXT NOT NULL UNIQUE"),
                    Column("endtime", "TEXT NOT NULL UNIQUE"),
                    Column("duration", "REAL"),
                    Column("ocr_done_at", "TEXT"),
                    Column("created_at", "TEXT NOT NULL"),
                    Column("updated_at", "TEXT"),
                ],
                fts=False,
            ),
            Table(
                "active_windows",
                [
                    Column("window_id", "INTEGER NOT NULL", indexed=False),
                    Column("pid", "INTEGER NOT NULL", indexed=False),
                    Column("title", "TEXT NOT NULL"),
                    Column("appname", "TEXT NOT NULL"),
                    Column("path", "TEXT NOT NULL"),
                    Column("active_at", "TEXT NOT NULL UNIQUE", indexed=False),
                    Column("created_at", "TEXT NOT NULL", indexed=False,),
                ],
            ),
            Table(
                "ocr",
                [
                    Column("extracted_text", "TEXT NOT NULL"),
                    Column("timestamp", "TEXT NOT NULL UNIQUE", indexed=False),
                    Column("imagefile", "TEXT NOT NULL UNIQUE", indexed=False),
                    Column("segmentfile", "TEXT NOT NULL", indexed=False),
                    Column("created_at", "TEXT NOT NULL", indexed=False,),
                ],
            ),
        ]

    def _view_defs(self) -> List[str]:
        return [
            """
            CREATE VIEW IF NOT EXISTS active_windows_ranges AS
            SELECT rowid, active_windows.*, active_at AS starttime, LEAD(active_at, 1) OVER win AS endtime
            FROM active_windows
            WINDOW win AS (ORDER BY rowid ASC)
            """
        ]

    def remove_data_older_than(self, timestamp: datetime):
        statements = [
            """DELETE FROM active_windows WHERE created_at < ?""",
            """DELETE FROM ocr WHERE rowid in (
                   SELECT ocr.rowid
                   FROM ocr JOIN segments
                        ON ocr.segmentfile = segments.filename
                        AND segments.created_at < ?
            )""",
            """DELETE FROM segments WHERE created_at < ?""",
        ]
        for statement in statements:
            self.execute(statement, (timestamp,))

    def execute(self, statement: str, values: Iterable[Any] = []):
        values = [self._value_for_db(v) for v in values]
        return self.conn.execute(statement, values)

    def _value_for_db(self, v):
        if isinstance(v, datetime):
            assert v.utcoffset() == timedelta(0), "Must be UTC"
            return v.astimezone(pytz.UTC).isoformat()
        else:
            return v

    def close(self):
        print("Closing sqlite connection")
        self.conn.close()
