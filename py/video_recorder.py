#!python

from __future__ import annotations

import asyncio
import platform
import re
import subprocess
from datetime import datetime, timedelta
from functools import cached_property
from pathlib import Path
from typing import List, Optional, Tuple

import aiorun
import attr
import click
from devtools import debug
from pytz import utc
from tzlocal import get_localzone

from monitored_subprocess import MonitoredSubprocess, make_options_list


def linux_get_screen_info() -> Tuple[str, str, int, int]:
    xdpyinfo: subprocess.CompletedProcess = subprocess.run(
        ["xdpyinfo"], capture_output=True
    )
    xdpyinfo.check_returncode()
    match = re.search(
        r"name of display:\s*(?P<display_name>:\d+).*?screen #(?P<screen_number>\d+):\s*dimensions:\s*(?P<width>\d+)x(?P<height>\d+) pixels",
        xdpyinfo.stdout.decode("utf-8"),  # Assuming utf-8
        re.DOTALL,
    )
    if match:
        return (
            match.group("display_name"),
            match.group("screen_number"),
            int(match.group("width")),
            int(match.group("height")),
        )
    else:
        raise RuntimeError("xdpyinfo failed")


def _get_input_device() -> List[str]:
    if platform.system() == "Linux":
        # http://ffmpeg.org/ffmpeg-devices.html#x11grab
        #
        #
        # format of input is `[hostname]:display_number.screen_number[+x_offset,y_offset]`
        #
        # - hostname is optional
        # - display_number is in $DISPLAY
        #
        # - We're recording screen 0 by default, but parsing `xdpyinfo` might be the way to go
        #
        # - video_size is full desktop by default. x11grab doesn't
        #   support individual window grabbing, but see
        #   https://superuser.com/a/1378465 for how to do this with gstreamer
        #
        # - See h264/h265 options at https://trac.ffmpeg.org/wiki/Encode/H.265

        (display_name, screen_number, width, height) = linux_get_screen_info()

        return make_options_list(
            video_size=f"{width}x{height}",
            framerate=10,
            f="x11grab",
            i=f"{display_name}.{screen_number}",
        )
    else:
        # https://trac.ffmpeg.org/wiki/Capture/Desktop
        raise NotImplementedError("Windows and Mac not yet supported")


def _nice_options() -> List[str]:
    return ["nice"]


def _construct_ffmpeg_options(
    output_directory: Path, segment_duration: int
) -> List[str]:
    opts = _get_input_device()
    strftime_template = "%Y-%m-%d-%H-%M-%S"
    opts += make_options_list(
        # This should eventually be DASH or HLS, but for simplicity
        strftime=1,
        loglevel="warning",
        segment_time=segment_duration,
        segment_atclocktime=0,
        f="segment",
        **{"c:v": "libx265", "x265-params": "log-level=warning"},
    ) + [f"{output_directory}/seg-{strftime_template}.mp4"]
    return _nice_options() + ["ffmpeg", "-hide_banner"] + opts


@attr.s
class VideoRecorder:
    output_directory: Path = attr.ib(kw_only=True)
    segment_duration: int = attr.ib(kw_only=True, default=60)
    _cmdline: List[str] = attr.ib(init=False)
    _proc: Optional[MonitoredSubprocess] = attr.ib(init=False, default=None)

    @output_directory.validator
    def _check_output_directory(self, attribute, value: Path):
        assert value.exists() and value.is_dir(), "Directory exists"

    def __attrs_post_init__(self) -> None:
        self._cmdline = _construct_ffmpeg_options(
            self.output_directory, self.segment_duration
        )

    async def start(self) -> None:
        self._stopped = False
        assert self._proc is None, "Not recording"
        print("VideoRecorder: Recording")
        self._proc = MonitoredSubprocess(
            "ffmpeg",
            await asyncio.create_subprocess_exec(
                *self._cmdline,
                stdin=asyncio.subprocess.DEVNULL,
                stderr=(self.output_directory / "ffmpeg.stderr").open("a"),
            ),
        )
        await self.check()

    async def check(self) -> None:
        if self._proc is not None:
            await self._proc.check()

    async def stop(self) -> None:
        assert self._proc is not None, "Recording"
        print("VideoRecorder: Stopping")
        await self._proc.stop()
        self._proc = None

    @property
    def is_active(self) -> bool:
        return self._proc is not None


async def main(output_directory: Path, segment_duration: int) -> None:
    try:
        vr = VideoRecorder(
            output_directory=output_directory, segment_duration=segment_duration
        )
        await vr.start()
        asyncio.get_event_loop().stop()
    except asyncio.CancelledError:
        await vr.stop()


def segment_file_duration(segment_file: Path) -> float:
    cmd = ["ffprobe"] + make_options_list(
        v="error",
        show_entries="format=duration",
        of="default=noprint_wrappers=1:nokey=1",
    )
    proc = subprocess.run(
        cmd + [str(segment_file)],
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.DEVNULL,
    )
    return float(proc.stdout)


@attr.s
class SegmentFile:
    filename: Path = attr.ib()
    starttime: datetime = attr.ib(init=False)

    @filename.validator
    def _validate_filename(self, attribute, value: Path):
        assert value.exists(), "Exists"
        assert value.is_file(), "Is file"

    def __attrs_post_init__(self):
        # force naive timestamp as local timezone
        # This will retain microsecond precision
        self.starttime = datetime.fromtimestamp(
            self.filename.stat().st_ctime_ns / 1e9, tz=get_localzone()
        ).astimezone(utc)

    @cached_property
    def duration(self):
        return segment_file_duration(self.filename)

    @property
    def endtime(self):
        return self.starttime + timedelta(seconds=self.duration)

    @property
    def is_closed(self):
        try:
            self.duration
        except subprocess.CalledProcessError:
            return False
        else:
            return True


@click.command()
@click.argument("output_directory", type=click.Path(file_okay=False, dir_okay=True))
@click.option("--segment_duration", type=int, default=60, show_default=True)
def record_segments(output_directory: str, segment_duration: int) -> None:
    aiorun.run(
        main(Path(output_directory), segment_duration), stop_on_unhandled_errors=True
    )


if __name__ == "__main__":
    record_segments()
