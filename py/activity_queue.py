import asyncio
from datetime import datetime
from typing import List, NoReturn, Optional

import attr
from async_timeout import timeout
from colorama import Fore


@attr.s
class ActivityQueue:
    queue: asyncio.Queue = attr.ib(factory=asyncio.Queue, init=False)
    events_queue = attr.ib(kw_only=True)
    inactivity_window: float = attr.ib(kw_only=True)

    @staticmethod
    def _timenow() -> float:
        return datetime.now().timestamp()

    async def run(self) -> NoReturn:
        latest_event_time = self._timenow()
        while True:
            latest_event_time = await self._get_latest_event_time() or latest_event_time

            timenow = self._timenow()
            elapsed_since_latest_event = timenow - latest_event_time

            if elapsed_since_latest_event >= self.inactivity_window:
                await self.queue.put(
                    (
                        timenow,
                        "inactivity",
                        latest_event_time,
                        elapsed_since_latest_event,
                    )
                )
            else:
                await self.queue.put(
                    (timenow, "activity", latest_event_time, elapsed_since_latest_event)
                )
                await asyncio.sleep(self.inactivity_window - elapsed_since_latest_event)

    async def _get_latest_event_time(self) -> Optional[float]:
        """
        Drain the queue and return the most recent event's timestamp, or None
        if the queue is empty for self.inactivity_window
        """
        latest_ts: Optional[float] = None
        try:
            while True:
                async with timeout(self.inactivity_window):
                    latest_ts, *_ = await self.events_queue.get()
                    if self.events_queue.empty():
                        return latest_ts
        except asyncio.TimeoutError:
            return latest_ts


def async_q_tee(inq: asyncio.Queue, n=2) -> List[asyncio.Queue]:
    """
    Clones inq to n queues so that the items can be consumed multiple times independently

    this is probably nicer as a class so we don't have to do
    create_task() and instead can await on something

    """

    outqs: List[asyncio.Queue] = [asyncio.Queue() for i in range(n)]

    async def _tee() -> NoReturn:
        while True:
            item = await inq.get()
            await asyncio.gather(*(q.put(item) for q in outqs))

    asyncio.get_event_loop().create_task(_tee())
    return outqs


async def log_inactivity(q) -> NoReturn:
    while True:
        evt = await q.get()
        if evt[1] == "inactivity":
            print(Fore.MAGENTA + f"{evt=}")


async def log_activity(q) -> NoReturn:
    while True:
        evt = await q.get()
        if evt[1] == "activity":
            print(Fore.CYAN + f"{evt=}")
