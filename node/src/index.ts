import activeWin from 'active-win';
import { promisify } from 'util';
import yargs from 'yargs';
import axios from 'axios';
const sleep = promisify(setTimeout);

const argv = yargs
  .option('p', {
    nargs: 1,
    type: 'number',
    alias: 'port',
    default: 8000,
  })
  .option('d', { type: 'boolean', alias: 'debug', default: false, nargs: 1 }).argv;

const apiURL = `http://localhost:${argv.port}`;

type Window = {
  window_id: number;
  pid: number;
  title: string;
  appname: string;
  path: string;
};

const postActiveWin = async (win: Window) => {
  if (argv.debug) {
    console.log(win);
  }
  const response = await axios.post(`${apiURL}/active_window/`, {
    ...win,
    active_at: new Date().toISOString(),
  });
  if (response.status !== 200) {
    throw new Error('Failed POST');
  }
};

const pollActiveWin = async () => {
  let prev: activeWin.BaseResult | null = null;
  while (true) {
    const current = await activeWin();
    const change =
      (prev === null && current !== undefined) || // Nothing -> Something
      (prev !== null && current === undefined) || // Something -> Nothing
      (prev !== null &&
        current !== undefined &&
        (prev.id !== current.id || prev.title !== current.title)); // Something -> Something else

    if (!change) {
      continue;
    }
    if (current === undefined) {
      await postActiveWin({
        window_id: -1,
        pid: -1,
        title: '',
        appname: '',
        path: 'no_active_window',
      });
      prev = null;
    } else {
      await postActiveWin({
        window_id: current.id,
        pid: current.owner.processId,
        title: current.title,
        appname: current.owner.name,
        path: current.owner.path,
      });
      prev = current;
    }

    await sleep(5000);
  }
};

(async function main() {
  await pollActiveWin();
})();
